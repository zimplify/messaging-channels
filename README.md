# In-app Messaging implementation for Zimplify App
This feature enable you to message the other party but not as much as in RTC. There needs to be additional consideration to consider when in a real-time mode, such as:
* Letting the other party know data is being input
* The message is getting "transported" once is sent.
As such, this implementation is more of a wall type messaging that you see in facebook.
(We will look into the RTC option soon as soon as we got other features out the way... plus we need to setup a websocket server to enable this...)

## Installation
Simply activate via composer
````
composer require zimplify\app-messager
````

## Configuration
There are no specific environment setup needed for this module

## Usage
The module consist of the main class call `Channel`, which has participants inside. Each participants (if the group is private - will check if the participant is allowed) will broadcast their message via a `say()` method. 

In the case reading the messages, there is a `digest` attribute that will read out the messages in pages. In which each message (or `Whisper`) contains a `wrote` attribute to indicate whether the message is from the user so you can easily use it for formatting.

Here are the key methods within the `Channel` class:

### dismiss(IObjectAuthorInterface $participant, IObjectAuthorInterface $replacement = null)
This method removes the `$participant` out from the channel. In the case the `$participant` is the `$moderator` of the channel, the `$replacement` may be used (or the next oldest partcipant) will assumes its place. If the `$participant` is the last in the channel. The whole channel will be destroyed, along with all its messages inside.

### say(IObjectAuthorInterface $sender, string $message, File $attachment = null)
This method sends broadcast to the channels and all its participants will be able see the message. In the next release, the `$sender` will become obsolete as it is taken care via the `withAudience()` method.

### withAudience(IObjectAuthorInterface $whisper)
This method identifies the person is tapping onto the channel

### withParticipant(IObjectAuthorInterface $participant)
Adding new participants into the channel.

### withPrivacy(bool $mode)
Setting whether the channel is private (when set to `true`) or public (when set to `false`). In the case of private channel, all messages are checked and 
make sure the sender is a participant before issue the save onto the channel.

### withTitle(string $title)
Setting the title of the channel. The title is required before the channel can be active.