<?php
    namespace Zimplify\AppMessaging\Providers;
    use Zimplify\Core\{Application, Message, Provider};
    use Zimplify\Core\Interfaces\{IObjectAuthorInterface, IMessageDispatchableInterface};
    use \RuntimeException;
    
    /**
     * the InAppProvider helps us to save and load messages if required
     * @package Zimplify\AppMessaging (code 12)
     * @type Provider (code 03)
     * @file InAppProvider (code 01)
     */
    class InAppProvider extends Provider {
    
        const DEF_CLS_NAME = self::class;
        const DEF_SHT_NAME = "core-appmsg::in-app";
    
        /**
         * initializing the provider.
         * @return void
         */
        protected function initialize() {
        }
    
        /**
         * check if all required arguments are supplied into the provider for initialization.
         * @return bool
         */
        protected function isRequired() : bool {
            return true;
        }

        /**
         * sending out the message via the carrier
         * @param Message $message the message to send
         * @return bool
         */
        function send(Message $message) : bool {
            return true;
        }
    }
    