<?php
    namespace Zimplify\AppMessaging;
    use Zimplify\Core\{Application, File, Instance, Query, Search};
    use Zimplify\Core\Interfaces\IObjectAuthorInterface;
    use Zimplify\AppMessaging\Whisper;
    use Zimplify\Core\Traits\TSearchPagingHandler;
    use \RuntimeException;

    /**
     * <add class description>
     * @package Zimplify\AppMessaging (code 12)
     * @type instance (code 01)
     * @file Channel (code 01)
     */
    class Channel extends Instance {
        use TSearchPagingHandler;

        // our class constants
        const DEF_CLS_NAME = self::class;
        const DEF_SHT_NAME = "core-appmsg::channel";
        const ERR_NOT_ALLOWED = 403120101001;
        const FLD_AUDIENCE = "audience";
        const FLD_MESSAGES = "messages";
        const FLD_DESCRIPTION = "description";
        const FLD_DIGEST = "digest";
        const FLD_PRIVATE_MODE = "mode";
        const FLD_PARTICIPANTS = "participants";
        const FLD_TITLE = "title";

        private $perspective;
        private $pagesize = 10;
        
        /**
         * the magic get override method
         * @param string $param the parameter to read
         * @return mixed
         */
        public function __get(string $param) {
            $result = null;
            switch ($param) {
                case self::FLD_AUDIENCE: 
                    $result = [];
                    foreach ($this->{self::FLD_PARTICIPANTS} as $audience) {
                        $search = (new Search("objects"))
                            ->withCondition(Query::SRF_ID, $audience)
                            ->withCondition(Query::SRF_STATUS, true)
                            ->run();
                        if (count($search) > 0 && is_a($search[0], IObjectAuthorInterface::class)) 
                            array_push($result, $search[0]);
                        else {
                            $depart = (new Search("objects"))->withCondition(Query::SRF_ID, $audience)->run();
                            if (count($depart) > 0 && is_a($search[0], IObjectAuthorInterface::class)) 
                                $this->dismiss($depart[0]);
                        }                            
                    }
                    break;
                case self::FLD_MESSAGES:
                    $result = (new Search("messages"))
                        ->withCondition(Query::SRF_TYPE, Whisper::DEF_SHT_NAME)
                        ->withCondition(Query::SRF_PARTNER, $this->_id)
                        ->run();
                    break;
                case self::FLD_DIGEST:
                    $search = (new Search("messages"))
                        ->withCondition(Query::SRF_TYPE, Whisper::DEF_SHT_NAME)
                        ->withCondition(Query::SRF_STATUS, true)
                        ->withCondition(Query::SRF_PARTNER, $this->_id)
                        ->withSort(self::FLD_CREATED, true)
                        ->withPageSize($this->pagesize)
                        ->withPageOffset($this->offset)
                        ->run(); 
                    $result = $this->asAudience($search);                     
                    break;
                default: $result = parent::__get($param);
            }
            return $result;
        }

        /**
         * filtering the messages and make sure it is in the view point of the audience
         * @param array $whispers the message to align
         * @return array
         */
        protected function asAudience(array $whispers) : array { 
            $result = [];
            if (!is_null($this->perspective)) {
                foreach ($whispers as $whisper) {
                    if ($whisper->parent()->id == $this->perspective->id) 
                        $whisper->{Whisper::FLD_WROTE} = true;
                    array_push($result, $whisper);
                }    
            } else 
                $result = $whispers;
            return $result;
        }

        /**
         * maksing sure our moderator is part of the participants
         * @return void
         */
        public function assume() : void {
            if (!is_null($this->parent())) $this->withParticipant($this->parent());
        }

        /**
         * remove a participant from the channel. if the departer is the moderator (parent), it will transfer to the next oldest participant
         * @param IObjectAuthorInterface $departer the person to leave the channel
         * @param IObjectAuthorInterface $replacement (optional) the dedicate moderator successor. If not provider, will be the oldest
         * @return Channel
         */
        public function dismiss(IObjectAuthorInterface $departer, IObjectAuthorInterface $replacement = null) : self {
            if (in_array($departer->id, ($participants = $this->{self::FLD_PARTICIPANTS}))) {
                if (count($participants) > 1) {
                    $index = array_search($departer->id, $participants);
                    array_splice($participants, $index, 1);                    
                    if ($departer->id === $this->parent()->id) {
                        $kin = !is_null($replacement) && in_array($replacement->id, $this->{self::FLD_PARTICIPANTS}) ? 
                            $replacement : (new Search("objects"))->withCondition(Query::SRF_ID, $participants[0]);
                        $this->transfer($kin);
                    }
                    $this->{self::FLD_PARTICIPANTS} = $participants;
                } else 
                    $this->delete();
            } 
            return $this;
        }

        /**
         * check if the announcer (the person who want to add to channel) is allowed
         * @param IObjectAuthorInterface $announcer the person to check
         * @return bool
         */
        public function isAllowed(IObjectAuthorInterface $announcer) : bool {
            $result = true;
            if ($this->{self::FLD_PRIVATE_MODE} === true) 
                $result = in_array($announcer->id, $this->{self::FLD_PARTICIPANTS});                    
            return $result;
        }

        /**
         * check if the current write can moderate this channel
         * @return bool
         */
        public function isModeratable() : bool {
            $result = false;
            if (!is_null($this->perspective) && !is_null($this->parent())) 
                $result = $this->perspective === $this->parent()->id;
            return $result;
        }
        
        /**
         * our instance initialization routine (like envokes, etc)
         * @return void
         */
        protected function prepare() {
            $this->withEventHandler(self::EVENT_BEFORE_SAVE, array($this, "assume"));
        }

        /**
         * the default onInstanceDelete of deleted event
         * @return void
         */
        protected function onInstanceDelete() : void {
            if (!$this->isModeratable()) {
                foreach ($this->{self::FLD_MESSAGES} as $message)
                    $message->withAudience($this->parent())->delete();
                parent::onInstanceDelete();            
            } else 
                throw new RuntimeException("Channel is only removable by moderator", self::ERR_NOT_ALLOWED);
        }
        
        /**
         * adding the whisper to the conversation
         * @param IObjectAuthorInterface $sender the person who wrote this message
         * @param File $attachment (optional) resource we are linking with the message
         * @return Whisper         
         */
        public function say(IObjectAuthorInterface $sender, string $message, File $attachment = null) : Whisper {            
            if ($this->isAllowed($sender) === true) {
                $this->withParticipant($sender);
                $whisper = Application::create(Whisper::DEF_SHT_NAME, $sender);
                $this->debug("running assign");
                $whisper = $whisper->assign($this);
                $this->debug("end running assign");
                $whisper = $whisper->withBody($message);
                if (!is_null($attachment))
                    if (!is_null($attachment->location) && !is_null($attachment->mime)) 
                        $whisper = $whisper->withHyperlink($attachment->location, $attachment->mime);
                $whisper->dispatch();
                return $$whisper;
            } else 
                throw new RuntimeException("This user is not allow to participant in this channel.", self::ERR_NOT_ALLOWED);
        }

        /**
         * apply the perspective into the channel as audience
         * @param IObjectAuthorInterface $person the person to participate
         * @return Channel
         */
        public function withAudience(IObjectAuthorInterface $person) : self {
            $this->perspective = $person;
            return $this;
        }

        /**
         * adding new participant into the channel list
         * @param IObjectAuthorInterface $participant the agent to add to group
         * @return Channel
         */
        public function withParticipant(IObjectAuthorInterface $participant) : self {
            $participants = $this->{self::FLD_PARTICIPANTS} ?? [];
            if (!in_array($participant->id, $participants)) 
                array_push($participants, $participant->id);
            $this->{self::FLD_PARTICIPANTS} = $participants;
            return $this;
        }

        /**
         * set the pagesize of the output of whispers
         * @param int $size the size to output
         * @return Channel
         */
        public function withPageSize(int $size) : self {
            if ($size > 0) $this->pagesize = $size;
            return $this;
        }

        /**
         * update the privacy exposure of the channel
         * @param bool $mode the intended mode of operation
         * @return Channel
         */
        public function withPrivacy(bool $mode) : self {
            $this->{self::FLD_PRIVATE_MODE} = $mode;
            return $this;
        }

        /**
         * set the title of the channel
         * @param string $title the title to apply
         * @return Channel
         */
        public function withTitle(string $title) : self {
            $this->{self::FLD_TITLE} = $title;
            return $this;
        }
    }