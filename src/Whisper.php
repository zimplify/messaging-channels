<?php
    namespace Zimplify\AppMessaging;
    use Zimplify\Core\{Application, Message, Provider, Query, Search};
    use Zimplify\Core\Interfaces\{IObjectAuthorInterface, IPartnershipInterface};    
    use Zimplify\Core\Traits\TPartnerHandler;
    use Zimplify\AppMessaging\Channel;
    use Zimplify\AppMessaging\Providers\InAppProvider;
    use \RuntimeException;
    /**
     * Whisper is the conversation's messaging unit allow us to track back to where things are started
     * @package Zimplify\Messaging (code 03)
     * @type instance (code 01)
     * @file Whisper (code 08)
     */
    class Whisper extends Message implements IPartnershipInterface {
        use TPartnerHandler;

        // our class constants
        const DEF_CLS_NAME = self::class;
        const DEF_MSG_TYPE = "whisper";
        const DEF_SHT_NAME = "core-appmsg::whisper";
        const ERR_NOT_ALLOWED = 403030108001;
        const FLD_HYPERLINK = "hyperlink";
        const FLD_HYPERLINK_TYPE = "hyperlink.mime";
        const FLD_HYPERLINK_VALUE = "hyperlink.value";
        const FLD_WROTE = "wrote";

        /**
         * getting the carrier for the message
         * @return Provider
         */
        protected function getCarrier() : Provider {
            return Application::request(InAppProvider::DEF_SHT_NAME, []);            
        }        

        /**
         * get the type of message in order to extract the template path
         * @return string
         */
        protected function getMessageType() : string {
            return self::DEF_MSG_TYPE;
        }

        /**
         * check if the message needs receiver to send
         * @return bool
         */
        protected function isRequiredAudience() : bool {
            return false;
        }        

        /**
         * our instance initialization routine (like envokes, etc)
         * @return void
         */
        protected function prepare() {
            $this->withPersistable(true);
            $this->withSenderProtection(true);
        }

        /**
         * handing if someone (either moderator or author) to remove the request
         * @param IObjectAuthorInterface $requester the consumer/user that request the change
         * @return Whisper
         */
        public function invalidate(IObjectAuthorInterface $requester) : self {
            if ($requester === $this->parent() || $requester === $this->partner()->parent()) {
                $this->{self::FLD_STATUS} = false;
                $this->save();
            } else 
                throw new RuntimeException("Requester is not allowed to do this.", self::ERR_NOT_ALLOWED);
            return $this;
        }

        /**
         * attaching a resource location to display
         * @param string $location the place where the reosurce is
         * @param string $type (optional) the MIME reference from the hyperlink
         * @return Whisper
         */
        public function withHyperlink(string $location, string $type = null) : self {
            $this->{self::FLD_HYPERLINK_VALUE} = $this->location;
            $this->{self::FLD_HYPERLINK_TYPE} = $type;
            return $this;   
        }
    }